export class Beer {
    private _name = '';
    private _tagline = '';
    private _firstBrewed = '';
    private _description = '';
    private _imageUrl = '';
    private _abv = 0;

    constructor(beer?: any) {
        if (beer !== undefined) {
            this._name = beer.name;
            this._tagline = beer.tagline;
            this._firstBrewed = beer.first_brewed;
            this._description = beer.description;
            this._imageUrl = beer.image_url;
            this._abv = beer.abv;
        }
    }

    get name(): string {
        return this._name;
    }

    get tagline(): string {
        return this._tagline;
    }

    get firstBrewed(): string {
        return this._firstBrewed;
    }

    get description(): string {
        return this._description;
    }

    get imageUrl(): string {
        return this._imageUrl;
    }

    get abv(): number {
        return this._abv;
    }

    set name(name: string) {
        this._name = name;
    }

    set tagline(tagline: string) {
        this._tagline = tagline;
    }

    set firstBrewed(firstBrewed: string) {
        this._firstBrewed = firstBrewed;
    }

    set description(description: string) {
        this._description = description;
    }

    set imageUrl(imageUrl: string) {
        this._imageUrl = imageUrl;
    }

    set abv(abv: number) {
        this._abv = abv;
    }

}