import { calculatorStore } from "../store/store";

export enum State {
    INIT,
    FIRST_OPERAND,
    SECOND_OPERAND,
    RESULT,
}

export class CalculatorModel {

    private currentState = State.INIT;
    private firstOperand = 0;
    private secondOperand = 0;
    private operator = "";
    private result = 0;
    public display = "";

    constructor() {
        calculatorStore.subscribe((value) => {
            this.currentState = value.currentState;
            this.firstOperand = value.firstOperand;
            this.secondOperand = value.secondOperand;
            this.operator = value.operator;
            this.result = value.result;
            this.display = value.display;
        });
    }

    updateStore() {
        calculatorStore.set({
            currentState: this.currentState,
            firstOperand: this.firstOperand,
            secondOperand: this.secondOperand,
            operator: this.operator,
            result: this.result,
            display: this.display,
        });
    }

    handleNumber(value: number): string {
        switch (this.currentState) {
            case State.INIT:
                this.firstOperand = value;
                this.currentState = State.FIRST_OPERAND;
                this.display = this.firstOperand.toString();
                break;
            case State.FIRST_OPERAND:
                this.firstOperand = this.firstOperand * 10 + value;
                this.display += value.toString();
                break;
            case State.SECOND_OPERAND:
                this.secondOperand = this.secondOperand * 10 + value;
                this.display += value.toString();
                break;
            case State.RESULT:
                this.firstOperand = value;
                this.secondOperand = 0;
                this.operator = '';
                this.result = 0;
                this.currentState = State.FIRST_OPERAND;
                this.display = this.firstOperand.toString();
                break;
            default:
                break;
        }
        this.updateStore();
        return this.display;
    }

    private clearCalculator() {
        this.display = "";
        this.currentState = State.INIT;
        this.firstOperand = 0;
        this.secondOperand = 0;
        this.operator = "";
        this.result = 0;
        this.updateStore();
    }

    handleSymbol(value: string): string {
        if (value === "C") {
            this.clearCalculator();
            return '';
        }
        switch (this.currentState) {
            case State.INIT:
                break;
            case State.FIRST_OPERAND:
                if(this.isOperator(value)){
                    this.operator = value;
                    this.currentState = State.SECOND_OPERAND;
                    this.display += value;
                }
                break;
            case State.SECOND_OPERAND:
                if(value === '='){
                    this.result = this.resolve();
                    this.currentState = State.RESULT;
                    this.display += value + this.result;
                }
                break;
            case State.RESULT:
                if(this.isOperator(value)){
                    this.firstOperand = this.result;
                    this.secondOperand = 0;
                    this.operator = value;
                    this.result = 0;
                    this.currentState = State.SECOND_OPERAND;
                    this.display = this.firstOperand.toString() + this.operator;
                }
                break;
            default:
                break;
        }
        this.updateStore();
        return this.display;
    }

    private isOperator(value: string){
        return value === '+' || value === '-' || value === '*' || value === '/';
    }

    private resolve(){
        switch (this.operator) {
            case '+':
                return this.firstOperand + this.secondOperand;
            case '-':
                return this.firstOperand - this.secondOperand;
            case '*':
                return this.firstOperand * this.secondOperand;
            case '/':
                return this.firstOperand / this.secondOperand;
            default:
                return 0;
        }
    }



}