export class Apod {

    private _date!: string;
    private _explanation!: string;
    private _hdurl!: string;
    private _mediaType!: string;
    private _serviceVersion!: string;
    private _title!: string;
    private _url!: string;

    constructor(date?:string){
        
    }

    get date(): string {
        return this._date;
    }

    get explanation(): string {
        return this._explanation;
    }

    get hdurl(): string {
        return this._hdurl;
    }

    get mediaType(): string {
        return this._mediaType;
    }

    get serviceVersion(): string {
        return this._serviceVersion;
    }

    get title(): string {
        return this._title;
    }

    get url(): string {
        return this._url;
    }        

    setData(apod: any): void {
        this._date = apod.date;
        this._explanation = apod.explanation;
        this._hdurl = apod.hdurl;
        this._mediaType = apod.media_type;
        this._serviceVersion = apod.service_version;
        this._title = apod.title;
        this._url = apod.url;
    }

    getApod(date?: string): Promise<any> {
        const key = 'tqz634Z1x0LiJzjbhSyUoExrZaGKLM0MG1VnROR6';
        let url = `https://api.nasa.gov/planetary/apod?api_key=${key}`;
        if(date !== undefined){
            url += `&date=${date}`;
        }
        return fetch(url).then
        (response => response.json())
    }

    toString(): string {
        return `Apod: ${this._title} - ${this._date}`;
    }

    isImage(): boolean {
        return this._mediaType === 'image';
    }

    isVideo(): boolean {
        console.log(this._mediaType);
        
        return this._mediaType === 'video';
    }

}