export class Hero {

    constructor(private _name = '', private _description = '') {
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {       
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    toString(): string {
        return `Hero: ${this._name} - ${this._description}`;
    }

    clone (): Hero {
        return new Hero(this._name, this._description);
    }

    toJson(): any {
        return {
            name: this._name,
            description: this._description
        };
    }
}