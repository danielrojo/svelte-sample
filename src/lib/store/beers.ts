import { writable, get } from "svelte/store";
import { Beer } from "../models/beer";

export let beers = writable<Beer[]>([]);

export let range = writable<number[]>([2,5]);

export function updateRange(newRange: number[]) {
    range.set(newRange);
}

export function updateBeersReducer() {
    if (get(beers).length > 0) {
        return;
    }
    fetch('https://api.punkapi.com/v2/beers')
        .then(res => res.json())
        .then(data => {
            let myBeers: Beer[] = [];
            data.forEach((beer: Beer) => {
                let mybeer = new Beer(beer);
                myBeers.push(mybeer);
            });
            console.log(myBeers);
            
            beers.set(myBeers);
        });
}