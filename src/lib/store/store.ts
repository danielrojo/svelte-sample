import { readable, writable } from "svelte/store";
import { State } from "../models/calculator";
import { Hero } from "../models/hero";

export let counter = writable(0);
export let name = readable('world');

export let calculatorStore = writable({
    currentState: State.INIT,
    firstOperand: 0,
    secondOperand: 0,
    operator: "",
    result: 0,
    display: "",
});

export let heroes = writable<Hero[]>([new Hero("Superman", "MOF")]);



export function increment() {
    counter.update(n => n + 1);
}

export function decrement() {    
    counter.update(n => n - 1);
}

export function reset() {
    counter.set(0);
}

export function setCount(n: number) {
    counter.set(n);
}

export function addCount(n: number) {
    counter.update(c => c + n);
}

export function addHero(hero: Hero) {
    heroes.update(h => [...h, hero]);
}
